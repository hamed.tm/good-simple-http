'use strict';
const Stream        = require('stream');
const SafeStringify = require('fast-safe-stringify');
const Wreck         = require('wreck');

const defaults = {
	wreck: {
		timeout: 60000,
		headers: {}
	}
};

class GoodSimpleHttp extends Stream.Writable {
	constructor(endpoint, config) {
		config         = config || {};
		const settings = Object.assign({}, defaults, config);
		super({objectMode: true, decodeStrings: false});
		this._settings = settings;
		this._endpoint = endpoint;
		this._data     = {};

		this.once('finish', () => {
			this._send();
		});
	}

	_write(data, encoding, callback) {
		this._data = data;
		this._send((err) => {
			if (err) {
				return callback(err);
			}

			this._data = {};
			return callback();
		});
	}

	_send(callback) {
		const options = Object.assign({}, this._settings.wreck, {
			payload: SafeStringify(this._data)
		});

		options.headers['content-type'] = 'application/json';
		Wreck.request('post', this._endpoint, options, callback);
	}
}

module.exports = GoodSimpleHttp;